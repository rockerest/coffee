var express = require( "express" );
var { json } = require( "body-parser" );
var compression = require( "compression" );
var morgan = require( "morgan" );
var cors = require( "cors" );

var { join } = require( "path" );

var app = express();

const PORT = 80;

app.use( compression() );
app.use( morgan( "tiny" ) );

app.use( json() );

app.use( cors( { "origin": true } ) );
app.options( "*", cors( { "origin": true } ) );

app.use( express.static( "./", {
	"fallthrough": true
} ) );

app.use( ( req, res ) => {
	if( !res.headersSent ){
		res
			.status( 200 )
			.sendFile( join( process.cwd(), "/index.html" ) ); // eslint-disable-line no-undef
	}
} );


app.listen( PORT );

console.log( `http://localhost:${PORT}` );
