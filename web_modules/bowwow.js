var FetchClient;

function failsafeJson( string ){
    var json = string;
    var nonEmpty = ( str ) => str && str != "";

    if( nonEmpty( string ) ){
        try{
            json = JSON.parse( string );
        }
        catch( error ){
            json = string;
        }
    }

    return json;
}

function required( property, msg = "" ){
    throw Error( `Required named property "${property}" was not included or undefined. ${msg}` );
}

function isObject( val ){
    return val && typeof val == "object" && val !== null;
}

function isIterable( val ){
    return isObject( val ) && typeof val[ Symbol.iterator ] == "function";
}

function isLegacyError( status, config = {} ){
    var treatment = Object.assign( {}, { "failures": "legacy" }, config ).failures;
    var fourOrFive = ( /^[45]\d+/ ).test( status );
    var consideredError = false;

    if( treatment == "legacy" && fourOrFive ){
        consideredError = true;
    }

    return consideredError;
}

function simplifyHeaders( headersObject ){
    var headers = {};

    if( isIterable( headersObject )  ){
        for( let pair of headersObject.entries() ){
            headers[ pair[ 0 ] ] = pair[ 1 ];
        }
    }

    return headers;
}

function normalizeConfig( existing = {} ){
    return Object.assign( {}, {
        "getter": "text",
        "failures": "native",
        "defaultContent": true
    }, existing || {} );
}

function getUrl( url, config ){
    var urlRoot = "";

    if( config.urlRoot !== undefined && config.urlRoot !== null ){
        urlRoot = String( config.urlRoot );
    }

    return `${urlRoot}${url}`;
}

function getBody( body, config ){
    var stringify = config.stringifyBody !== false;
    var content = body || "";

    if( stringify ){
        content = JSON.stringify( body );
    }

    return content;
}

function getHeaders( headers, config ){
    if( !headers[ "Content-Type" ] && config.defaultContent ){
        headers[ "Content-Type" ] = "application/json;charset=UTF-8"; // default to JSON, that's what most people want
    }

    return new Headers( headers );
}

function getRequest( config ){
    var resolvedUrl = getUrl( config.url, config );
    var resolvedBody = getBody( config.body, config );
    var resolvedHeaders = getHeaders( config.headers || {}, config );

    var init = Object.assign( {}, config, {
        "method": String( config.method ).toUpperCase(),
        "url": resolvedUrl,
        "headers": resolvedHeaders
    } );

    if( ![ "GET", "HEAD" ].includes( init.method ) ){
        init.body = resolvedBody;
    }
    else{
        delete init.body;
    }

    return new Request( init.url, init );
}

function getResponseContent( response, config = {} ){
    var resolution;

    if( response[ config.getter ] ){
        try{
            resolution = response[ config.getter ]();
        }
        catch( error ){
            resolution = Promise.reject( error );
        }
    }
    else{
        resolution = Promise.reject( `No such Response method '${config.getter}'.` );
    }

    // By default, `text` is fetched, and it's then passed through the failsafe
    // JSON parser to try to always return something sensible
    return resolution.then( ( content ) => failsafeJson( content ) );
}

function getResponseObject( response = {} ){
    var obj = {
        "headers": {},
        "ok": undefined,
        "redirected": undefined,
        "status": undefined,
        "statusText": undefined,
        "type": undefined,
        "url": undefined
    };

    if( isObject( response ) ){
        obj.headers = simplifyHeaders( response.headers );
        obj.ok = response.ok;
        obj.redirected = response.redirected;
        obj.status = response.status;
        obj.statusText = response.statusText;
        obj.type = response.type;
        obj.url = response.url;
    }

    return obj;
}

function getStandardResponse( response, config ){
    var body = getResponseObject( response );
    var promise;

    if( response ){
        promise = getResponseContent( response, config );

        promise = promise.then(
            ( content ) => {
                let resolution;

                resolution = Object.assign( body, {
                    "content": content === undefined ? "" : content
                } );

                if( isLegacyError( response.status, config ) ){
                    resolution = Promise.reject( resolution );
                }

                return resolution;
            },
            ( error ) => Promise.reject( Object.assign( body, { error } ) )
        );
    }
    else{
        promise = Promise.reject( body );
    }

    return promise;
}

function getStandardRejection( response, config ){
    var body = getResponseObject( response );
    var promise = getResponseContent( response, config );

    return promise
        .then(
            ( content ) => Promise.reject( Object.assign( body, {
                "content": content === undefined ? "" : content
            } ) ),
            ( error ) => Promise.reject( Object.assign( body, { error } ) )
        );
}

FetchClient = {
    request( {
        method = required( "method", "'method' must be a valid HTTP method like GET, POST, PATCH, etc." ),  // eslint-disable-line no-unused-vars
        url = required( "url", "'url' must be a reachable destination url like 'http://www.google.com'" )   // eslint-disable-line no-unused-vars
    } = {} ){
        var config = normalizeConfig( Array.from( arguments )[ 0 ] );
        var req = getRequest( config );

        return fetch( req ).then(
            ( r ) => getStandardResponse( r, config ),
            ( r ) => getStandardRejection( r, config )
        );
    }
};

var FetchClient$1 = FetchClient;

export default FetchClient$1;
