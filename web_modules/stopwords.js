// http://www.damienvanholten.com/blog/dutch-stop-words/
var dutch_1 = [
	"aan",
	"af",
	"al",
	"alles",
	"als",
	"altijd",
	"andere",
	"ben",
	"bij",
	"daar",
	"dan",
	"dat",
	"de",
	"der",
	"deze",
	"die",
	"dit",
	"doch",
	"doen",
	"door",
	"dus",
	"een",
	"eens",
	"en",
	"er",
	"ge",
	"geen",
	"geweest",
	"haar",
	"had",
	"heb",
	"hebben",
	"heeft",
	"hem",
	"het",
	"hier",
	"hij ",
	"hoe",
	"hun",
	"iemand",
	"iets",
	"ik",
	"in",
	"is",
	"ja",
	"je ",
	"kan",
	"kon",
	"kunnen",
	"maar",
	"me",
	"meer",
	"men",
	"met",
	"mij",
	"mijn",
	"moet",
	"na",
	"naar",
	"niet",
	"niets",
	"nog",
	"nu",
	"of",
	"om",
	"omdat",
	"ons",
	"ook",
	"op",
	"over",
	"reeds",
	"te",
	"tegen",
	"toch",
	"toen",
	"tot",
	"u",
	"uit",
	"uw",
	"van",
	"veel",
	"voor",
	"want",
	"waren",
	"was",
	"wat",
	"we",
	"wel",
	"werd",
	"wezen",
	"wie",
	"wij",
	"wil",
	"worden",
	"zal",
	"ze",
	"zei",
	"zelf",
	"zich",
	"zij",
	"zijn",
	"zo",
	"zonder",
	"zou"
];

var dutch = {
	dutch: dutch_1
};

// via http://tedserbinski.com/files/stopwords.js.txt
var english_1 = [
  'a',
  'able',
  'about',
  'above',
  'abroad',
  'according',
  'accordingly',
  'across',
  'actually',
  'adj',
  'after',
  'afterwards',
  'again',
  'against',
  'ago',
  'ahead',
  'aint',
  'all',
  'allow',
  'allows',
  'almost',
  'alone',
  'along',
  'alongside',
  'already',
  'also',
  'although',
  'always',
  'am',
  'amid',
  'amidst',
  'among',
  'amongst',
  'an',
  'and',
  'another',
  'any',
  'anybody',
  'anyhow',
  'anyone',
  'anything',
  'anyway',
  'anyways',
  'anywhere',
  'apart',
  'appear',
  'appreciate',
  'appropriate',
  'are',
  'arent',
  'around',
  'as',
  'as',
  'aside',
  'ask',
  'asking',
  'associated',
  'at',
  'available',
  'away',
  'awfully',
  'b',
  'back',
  'backward',
  'backwards',
  'be',
  'became',
  'because',
  'become',
  'becomes',
  'becoming',
  'been',
  'before',
  'beforehand',
  'begin',
  'behind',
  'being',
  'believe',
  'below',
  'beside',
  'besides',
  'best',
  'better',
  'between',
  'beyond',
  'both',
  'brief',
  'but',
  'by',
  'c',
  'came',
  'can',
  'cannot',
  'cant',
  'cant',
  'caption',
  'cause',
  'causes',
  'certain',
  'certainly',
  'changes',
  'clearly',
  'cmon',
  'co',
  'co.',
  'com',
  'come',
  'comes',
  'concerning',
  'consequently',
  'consider',
  'considering',
  'contain',
  'containing',
  'contains',
  'corresponding',
  'could',
  'couldnt',
  'course',
  'cs',
  'currently',
  'd',
  'dare',
  'darent',
  'definitely',
  'described',
  'despite',
  'did',
  'didnt',
  'different',
  'directly',
  'do',
  'does',
  'doesnt',
  'doing',
  'done',
  'dont',
  'down',
  'downwards',
  'during',
  'e',
  'each',
  'edu',
  'eg',
  'eight',
  'eighty',
  'either',
  'else',
  'elsewhere',
  'end',
  'ending',
  'enough',
  'entirely',
  'especially',
  'et',
  'etc',
  'even',
  'ever',
  'evermore',
  'every',
  'everybody',
  'everyone',
  'everything',
  'everywhere',
  'ex',
  'exactly',
  'example',
  'except',
  'f',
  'fairly',
  'far',
  'farther',
  'few',
  'fewer',
  'fifth',
  'first',
  'five',
  'followed',
  'following',
  'follows',
  'for',
  'forever',
  'former',
  'formerly',
  'forth',
  'forward',
  'found',
  'four',
  'from',
  'further',
  'furthermore',
  'g',
  'get',
  'gets',
  'getting',
  'given',
  'gives',
  'go',
  'goes',
  'going',
  'gone',
  'got',
  'gotten',
  'greetings',
  'h',
  'had',
  'hadnt',
  'half',
  'happens',
  'hardly',
  'has',
  'hasnt',
  'have',
  'havent',
  'having',
  'he',
  'hed',
  'hell',
  'hello',
  'help',
  'hence',
  'her',
  'here',
  'hereafter',
  'hereby',
  'herein',
  'heres',
  'hereupon',
  'hers',
  'herself',
  'hes',
  'hi',
  'him',
  'himself',
  'his',
  'hither',
  'hopefully',
  'how',
  'howbeit',
  'however',
  'hundred',
  'i',
  'id',
  'ie',
  'if',
  'ignored',
  'ill',
  'im',
  'immediate',
  'in',
  'inasmuch',
  'inc',
  'inc.',
  'indeed',
  'indicate',
  'indicated',
  'indicates',
  'inner',
  'inside',
  'insofar',
  'instead',
  'into',
  'inward',
  'is',
  'isnt',
  'it',
  'itd',
  'itll',
  'its',
  'its',
  'itself',
  'ive',
  'j',
  'just',
  'k',
  'keep',
  'keeps',
  'kept',
  'know',
  'known',
  'knows',
  'l',
  'last',
  'lately',
  'later',
  'latter',
  'latterly',
  'least',
  'less',
  'lest',
  'let',
  'lets',
  'like',
  'liked',
  'likely',
  'likewise',
  'little',
  'look',
  'looking',
  'looks',
  'low',
  'lower',
  'ltd',
  'm',
  'made',
  'mainly',
  'make',
  'makes',
  'many',
  'may',
  'maybe',
  'maynt',
  'me',
  'mean',
  'meantime',
  'meanwhile',
  'merely',
  'might',
  'mightnt',
  'mine',
  'minus',
  'miss',
  'more',
  'moreover',
  'most',
  'mostly',
  'mr',
  'mrs',
  'much',
  'must',
  'mustnt',
  'my',
  'myself',
  'n',
  'name',
  'namely',
  'nd',
  'near',
  'nearly',
  'necessary',
  'need',
  'neednt',
  'needs',
  'neither',
  'never',
  'neverf',
  'neverless',
  'nevertheless',
  'new',
  'next',
  'nine',
  'ninety',
  'no',
  'nobody',
  'non',
  'none',
  'nonetheless',
  'noone',
  'no-one',
  'nor',
  'normally',
  'not',
  'nothing',
  'notwithstanding',
  'novel',
  'now',
  'nowhere',
  'o',
  'obviously',
  'of',
  'off',
  'often',
  'oh',
  'ok',
  'okay',
  'old',
  'on',
  'once',
  'one',
  'ones',
  'ones',
  'only',
  'onto',
  'opposite',
  'or',
  'other',
  'others',
  'otherwise',
  'ought',
  'oughtnt',
  'our',
  'ours',
  'ourselves',
  'out',
  'outside',
  'over',
  'overall',
  'own',
  'p',
  'particular',
  'particularly',
  'past',
  'per',
  'perhaps',
  'placed',
  'please',
  'plus',
  'possible',
  'presumably',
  'probably',
  'provided',
  'provides',
  'q',
  'que',
  'quite',
  'qv',
  'r',
  'rather',
  'rd',
  're',
  'really',
  'reasonably',
  'recent',
  'recently',
  'regarding',
  'regardless',
  'regards',
  'relatively',
  'respectively',
  'right',
  'round',
  's',
  'said',
  'same',
  'saw',
  'say',
  'saying',
  'says',
  'second',
  'secondly',
  'see',
  'seeing',
  'seem',
  'seemed',
  'seeming',
  'seems',
  'seen',
  'self',
  'selves',
  'sensible',
  'sent',
  'serious',
  'seriously',
  'seven',
  'several',
  'shall',
  'shant',
  'she',
  'shed',
  'shell',
  'shes',
  'should',
  'shouldnt',
  'since',
  'six',
  'so',
  'some',
  'somebody',
  'someday',
  'somehow',
  'someone',
  'something',
  'sometime',
  'sometimes',
  'somewhat',
  'somewhere',
  'soon',
  'sorry',
  'specified',
  'specify',
  'specifying',
  'still',
  'sub',
  'such',
  'sup',
  'sure',
  't',
  'take',
  'taken',
  'taking',
  'tell',
  'tends',
  'th',
  'than',
  'thank',
  'thanks',
  'thanx',
  'that',
  'thatll',
  'thats',
  'thats',
  'thatve',
  'the',
  'their',
  'theirs',
  'them',
  'themselves',
  'then',
  'thence',
  'there',
  'thereafter',
  'thereby',
  'thered',
  'therefore',
  'therein',
  'therell',
  'therere',
  'theres',
  'theres',
  'thereupon',
  'thereve',
  'these',
  'they',
  'theyd',
  'theyll',
  'theyre',
  'theyve',
  'thing',
  'things',
  'think',
  'third',
  'thirty',
  'this',
  'thorough',
  'thoroughly',
  'those',
  'though',
  'three',
  'through',
  'throughout',
  'thru',
  'thus',
  'till',
  'to',
  'together',
  'too',
  'took',
  'toward',
  'towards',
  'tried',
  'tries',
  'truly',
  'try',
  'trying',
  'ts',
  'twice',
  'two',
  'u',
  'un',
  'under',
  'underneath',
  'undoing',
  'unfortunately',
  'unless',
  'unlike',
  'unlikely',
  'until',
  'unto',
  'up',
  'upon',
  'upwards',
  'us',
  'use',
  'used',
  'useful',
  'uses',
  'using',
  'usually',
  'v',
  'value',
  'various',
  'versus',
  'very',
  'via',
  'viz',
  'vs',
  'w',
  'want',
  'wants',
  'was',
  'wasnt',
  'way',
  'we',
  'wed',
  'welcome',
  'well',
  'well',
  'went',
  'were',
  'were',
  'werent',
  'weve',
  'what',
  'whatever',
  'whatll',
  'whats',
  'whatve',
  'when',
  'whence',
  'whenever',
  'where',
  'whereafter',
  'whereas',
  'whereby',
  'wherein',
  'wheres',
  'whereupon',
  'wherever',
  'whether',
  'which',
  'whichever',
  'while',
  'whilst',
  'whither',
  'who',
  'whod',
  'whoever',
  'whole',
  'wholl',
  'whom',
  'whomever',
  'whos',
  'whose',
  'why',
  'will',
  'willing',
  'wish',
  'with',
  'within',
  'without',
  'wonder',
  'wont',
  'would',
  'wouldnt',
  'x',
  'y',
  'yes',
  'yet',
  'you',
  'youd',
  'youll',
  'your',
  'youre',
  'yours',
  'yourself',
  'yourselves',
  'youve',
  'z',
  'zero'
];

var english = {
	english: english_1
};

var french_1 = [
	'au',
	'aux',
	'avec',
	'ce',
	'ces',
	'dans',
	'de',
	'des',
	'du',
	'elle',
	'en',
	'et',
	'eux',
	'il',
	'je',
	'la',
	'le',
	'leur',
	'lui',
	'ma',
	'mais',
	'me',
	'même',
	'mes',
	'moi',
	'mon',
	'ne',
	'nos',
	'notre',
	'nous',
	'on',
	'ou',
	'par',
	'pas',
	'pour',
	'qu',
	'que',
	'qui',
	'sa',
	'se',
	'ses',
	'son',
	'sur',
	'ta',
	'te',
	'tes',
	'toi',
	'ton',
	'tu',
	'un',
	'une',
	'vos',
	'votre',
	'vous',
	'c',
	'd',
	'j',
	'l',
	'à',
	'm',
	'n',
	's',
	't',
	'y',
	'été',
	'étée',
	'étées',
	'étés',
	'étant',
	'suis',
	'es',
	'est',
	'sommes',
	'êtes',
	'sont',
	'serai',
	'seras',
	'sera',
	'serons',
	'serez',
	'seront',
	'serais',
	'serait',
	'serions',
	'seriez',
	'seraient',
	'étais',
	'était',
	'étions',
	'étiez',
	'étaient',
	'fus',
	'fut',
	'fûmes',
	'fûtes',
	'furent',
	'sois',
	'soit',
	'soyons',
	'soyez',
	'soient',
	'fusse',
	'fusses',
	'fût',
	'fussions',
	'fussiez',
	'fussent',
	'ayant',
	'eu',
	'eue',
	'eues',
	'eus',
	'ai',
	'as',
	'avons',
	'avez',
	'ont',
	'aurai',
	'auras',
	'aura',
	'aurons',
	'aurez',
	'auront',
	'aurais',
	'aurait',
	'aurions',
	'auriez',
	'auraient',
	'avais',
	'avait',
	'avions',
	'aviez',
	'avaient',
	'eut',
	'eûmes',
	'eûtes',
	'eurent',
	'aie',
	'aies',
	'ait',
	'ayons',
	'ayez',
	'aient',
	'eusse',
	'eusses',
	'eût',
	'eussions',
	'eussiez',
	'eussent',
	'ceci',
	'cela',
	'celà',
	'cet',
	'cette',
	'ici',
	'ils',
	'les',
	'leurs',
	'quel',
	'quels',
	'quelle',
	'quelles',
	'sans',
	'soi'
];

var french = {
	french: french_1
};

var german_1 = [
  'aber',
  'alle',
  'allem',
  'allen',
  'aller',
  'alles',
  'als',
  'also',
  'am',
  'an',
  'ander',
  'andere',
  'anderem',
  'anderen',
  'anderer',
  'anderes',
  'anderm',
  'andern',
  'anderr',
  'anders',
  'auch',
  'auf',
  'aus',
  'bei',
  'bin',
  'bis',
  'bist',
  'da',
  'dadurch',
  'daher',
  'damit',
  'dann',
  'darum',
  'das',
  'dass',
  'dasselbe',
  'dazu',
  'daß',
  'dein',
  'deine',
  'deinem',
  'deinen',
  'deiner',
  'deines',
  'dem',
  'demselben',
  'den',
  'denn',
  'denselben',
  'der',
  'derer',
  'derselbe',
  'derselben',
  'des',
  'deshalb',
  'desselben',
  'dessen',
  'dich',
  'die',
  'dies',
  'diese',
  'dieselbe',
  'dieselben',
  'diesem',
  'diesen',
  'dieser',
  'dieses',
  'dir',
  'doch',
  'dort',
  'du',
  'durch',
  'ein',
  'eine',
  'einem',
  'einen',
  'einer',
  'eines',
  'einig',
  'einige',
  'einigem',
  'einigen',
  'einiger',
  'einiges',
  'einmal',
  'er',
  'es',
  'etwas',
  'euch',
  'euer',
  'eure',
  'eurem',
  'euren',
  'eurer',
  'eures',
  'für',
  'gegen',
  'gewesen',
  'hab',
  'habe',
  'haben',
  'hat',
  'hatte',
  'hatten',
  'hattest',
  'hattet',
  'hier',
  'hin',
  'hinter',
  'ich',
  'ihm',
  'ihn',
  'ihnen',
  'ihr',
  'ihre',
  'ihrem',
  'ihren',
  'ihrer',
  'ihres',
  'im',
  'in',
  'indem',
  'ins',
  'ist',
  'ja',
  'jede',
  'jedem',
  'jeden',
  'jeder',
  'jedes',
  'jene',
  'jenem',
  'jenen',
  'jener',
  'jenes',
  'jetzt',
  'kann',
  'kannst',
  'kein',
  'keine',
  'keinem',
  'keinen',
  'keiner',
  'keines',
  'können',
  'könnt',
  'könnte',
  'machen',
  'man',
  'manche',
  'manchem',
  'manchen',
  'mancher',
  'manches',
  'mein',
  'meine',
  'meinem',
  'meinen',
  'meiner',
  'meines',
  'mich',
  'mir',
  'mit',
  'muss',
  'musst',
  'musste',
  'muß',
  'mußt',
  'müssen',
  'müßt',
  'nach',
  'nachdem',
  'nein',
  'nicht',
  'nichts',
  'noch',
  'nun',
  'nur',
  'ob',
  'oder',
  'ohne',
  'sehr',
  'seid',
  'sein',
  'seine',
  'seinem',
  'seinen',
  'seiner',
  'seines',
  'selbst',
  'sich',
  'sie',
  'sind',
  'so',
  'solche',
  'solchem',
  'solchen',
  'solcher',
  'solches',
  'soll',
  'sollen',
  'sollst',
  'sollt',
  'sollte',
  'sondern',
  'sonst',
  'soweit',
  'sowie',
  'um',
  'und',
  'uns',
  'unse',
  'unsem',
  'unsen',
  'unser',
  'unsere',
  'unses',
  'unter',
  'viel',
  'vom',
  'von',
  'vor',
  'wann',
  'war',
  'waren',
  'warst',
  'warum',
  'was',
  'weg',
  'weil',
  'weiter',
  'weitere',
  'welche',
  'welchem',
  'welchen',
  'welcher',
  'welches',
  'wenn',
  'wer',
  'werde',
  'werden',
  'werdet',
  'weshalb',
  'wie',
  'wieder',
  'wieso',
  'will',
  'wir',
  'wird',
  'wirst',
  'wo',
  'woher',
  'wohin',
  'wollen',
  'wollte',
  'während',
  'würde',
  'würden',
  'zu',
  'zum',
  'zur',
  'zwar',
  'zwischen',
  'über'
];

var german = {
	german: german_1
};

// via http://www.ranks.nl/stopwords/spanish.html
var spanish_1 = [
  'a',
  'un',
  'una',
  'unas',
  'unos',
  'uno',
  'sobre',
  'de',
  'todo',
  'también',
  'tras',
  'otro',
  'algún',
  'alguno',
  'alguna',
  'algunos',
  'algunas',
  'ser',
  'es',
  'soy',
  'eres',
  'somos',
  'sois',
  'esto',
  'estoy',
  'esta',
  'estamos',
  'estais',
  'estan',
  'como',
  'en',
  'para',
  'atras',
  'porque',
  'por qué',
  'estado',
  'estaba',
  'ante',
  'antes',
  'siendo',
  'ambos',
  'pero',
  'por',
  'no',
  'poder',
  'sal',
  'al',
  'puede',
  'puedo',
  'más',
  'ya',
  'le',
  'o',
  'me',
  'hasta',
  'durante',
  'ni',
  'ese',
  'contra',
  'eso',
  'mí',
  'mi',
  'el',
  'él',
  'podemos',
  'podeis',
  'pueden',
  'fui',
  'fue',
  'fuimos',
  'fueron',
  'hacer',
  'hago',
  'hace',
  'hacemos',
  'haceis',
  'hacen',
  'cada',
  'fin',
  'incluso',
  'primero',
  'desde',
  'conseguir',
  'consigo',
  'consigue',
  'consigues',
  'conseguimos',
  'consiguen',
  'ir',
  'voy',
  'va',
  'vamos',
  'vais',
  'van',
  'vaya',
  'gueno',
  'ha',
  'tener',
  'tengo',
  'tiene',
  'tenemos',
  'teneis',
  'tienen',
  'la',
  'lo',
  'las',
  'los',
  'su',
  'aqui',
  'mio',
  'poco',
  'tu',
  'tú',
  'te',
  'si',
  'sí',
  'tuyo',
  'ellos',
  'ella',
  'y',
  'del',
  'se',
  'ellas',
  'nos',
  'nosotros',
  'vosotros',
  'vosotras',
  'si',
  'dentro',
  'solo',
  'solamente',
  'saber',
  'sabes',
  'sabe',
  'sabemos',
  'sabeis',
  'saben',
  'ultimo',
  'largo',
  'bastante',
  'haces',
  'muchos',
  'aquellos',
  'aquellas',
  'sus',
  'entonces',
  'tiempo',
  'verdad',
  'verdadero',
  'verdadera',
  'cierto',
  'ciertos',
  'cierta',
  'ciertas',
  'intentar',
  'intento',
  'intenta',
  'intentas',
  'intentamos',
  'intentais',
  'intentan',
  'dos',
  'bajo',
  'arriba',
  'encima',
  'usar',
  'uso',
  'usas',
  'usa',
  'usamos',
  'usais',
  'usan',
  'emplear',
  'empleo',
  'empleas',
  'emplean',
  'ampleamos',
  'empleais',
  'valor',
  'muy',
  'era',
  'eras',
  'eramos',
  'eran',
  'modo',
  'bien',
  'cual',
  'cuando',
  'donde',
  'mientras',
  'quien',
  'con',
  'entre',
  'sin',
  'trabajo',
  'trabajar',
  'trabajas',
  'trabaja',
  'trabajamos',
  'trabajais',
  'trabajan',
  'podria',
  'podrias',
  'podriamos',
  'podrian',
  'podriais',
  'yo',
  'aquel',
  'que',
  '1','2','3','4','5','6','7','8','9','0'
];

var spanish = {
	spanish: spanish_1
};

var italian_1 = ["a",
 "abbastanza",
 "abbia",
 "abbiamo",
 "abbiano",
 "abbiate",
 "accidenti",
 "ad",
 "adesso",
 "affinche",
 "agl",
 "agli",
 "ahime",
 "ahimè",
 "ahimé",
 "ai",
 "al",
 "alcuna",
 "alcuni",
 "alcuno",
 "all",
 "alla",
 "alle",
 "allo",
 "allora",
 "altre",
 "altri",
 "altrimenti",
 "altro",
 "altrove",
 "altrui",
 "anche",
 "ancora",
 "anni",
 "anno",
 "ansa",
 "anticipo",
 "assai",
 "attesa",
 "attraverso",
 "avanti",
 "avemmo",
 "avendo",
 "avente",
 "aver",
 "avere",
 "averlo",
 "avesse",
 "avessero",
 "avessi",
 "avessimo",
 "aveste",
 "avesti",
 "avete",
 "aveva",
 "avevamo",
 "avevano",
 "avevate",
 "avevi",
 "avevo",
 "avra",
 "avrai",
 "avranno",
 "avrebbe",
 "avrebbero",
 "avrei",
 "avremmo",
 "avremo",
 "avreste",
 "avresti",
 "avrete",
 "avro",
 "avrà",
 "avrò",
 "avuta",
 "avute",
 "avuti",
 "avuto",
 "basta",
 "ben",
 "bene",
 "benissimo",
 "brava",
 "bravo",
 "buono",
 "c",
 "casa",
 "caso",
 "cento",
 "certa",
 "certe",
 "certi",
 "certo",
 "che",
 "chi",
 "chicchessia",
 "chiunque",
 "ci",
 "ciascuna",
 "ciascuno",
 "cima",
 "cinque",
 "cio",
 "cioe",
 "cioè",
 "cioé",
 "circa",
 "citta",
 "città",
 "ciò",
 "co",
 "codesta",
 "codesti",
 "codesto",
 "cogli",
 "coi",
 "col",
 "colei",
 "coll",
 "coloro",
 "colui",
 "come",
 "cominci",
 "comprare",
 "comunque",
 "con",
 "concernente",
 "conciliarsi",
 "conclusione",
 "consecutivi",
 "consecutivo",
 "consiglio",
 "contro",
 "cortesia",
 "cos",
 "cosa",
 "cosi",
 "così",
 "cui",
 "d",
 "da",
 "dagl",
 "dagli",
 "dai",
 "dal",
 "dall",
 "dalla",
 "dalle",
 "dallo",
 "dappertutto",
 "davanti",
 "debba",
 "degl",
 "degli",
 "dei",
 "del",
 "dell",
 "della",
 "delle",
 "dello",
 "dentro",
 "detto",
 "deve",
 "devi",
 "devo",
 "di",
 "dice",
 "dietro",
 "dire",
 "dirimpetto",
 "diventa",
 "diventare",
 "diventato",
 "dobbiamo",
 "dobbiate",
 "dopo",
 "doppio",
 "dov",
 "dove",
 "dovemmo",
 "dovendo",
 "dovere",
 "dovesse",
 "dovessero",
 "dovessi",
 "dovessimo",
 "doveste",
 "dovesti",
 "dovete",
 "dovette",
 "dovettero",
 "dovetti",
 "doveva",
 "dovevamo",
 "dovevano",
 "dovevate",
 "dovevi",
 "dovevo",
 "dovra",
 "dovrai",
 "dovranno",
 "dovrebbe",
 "dovrebbero",
 "dovrei",
 "dovremmo",
 "dovremo",
 "dovreste",
 "dovresti",
 "dovrete",
 "dovrà",
 "dovrò",
 "dovunque",
 "due",
 "dunque",
 "durante",
 "e",
 "ebbe",
 "ebbero",
 "ebbi",
 "ecc",
 "ecco",
 "ed",
 "effettivamente",
 "egli",
 "ella",
 "entrambi",
 "eppure",
 "era",
 "erano",
 "eravamo",
 "eravate",
 "eri",
 "ero",
 "esempio",
 "esse",
 "essendo",
 "esser",
 "essere",
 "essi",
 "ex",
 "fa",
 "faccia",
 "facciamo",
 "facciano",
 "facciate",
 "faccio",
 "facemmo",
 "facendo",
 "facesse",
 "facessero",
 "facessi",
 "facessimo",
 "faceste",
 "facesti",
 "faceva",
 "facevamo",
 "facevano",
 "facevate",
 "facevi",
 "facevo",
 "fai",
 "fanno",
 "farai",
 "faranno",
 "fare",
 "farebbe",
 "farebbero",
 "farei",
 "faremmo",
 "faremo",
 "fareste",
 "faresti",
 "farete",
 "farà",
 "farò",
 "fatto",
 "favore",
 "fece",
 "fecero",
 "feci",
 "fin",
 "finalmente",
 "finche",
 "fine",
 "fino",
 "forse",
 "forza",
 "fosse",
 "fossero",
 "fossi",
 "fossimo",
 "foste",
 "fosti",
 "fra",
 "frattempo",
 "fu",
 "fui",
 "fummo",
 "fuori",
 "furono",
 "futuro",
 "generale",
 "gente",
 "gia",
 "giacche",
 "giorni",
 "giorno",
 "giu",
 "già",
 "giã",
 "gli",
 "gliela",
 "gliele",
 "glieli",
 "glielo",
 "gliene",
 "governo",
 "grande",
 "grazie",
 "gruppo",
 "ha",
 "haha",
 "hai",
 "hanno",
 "ho",
 "i",
 "ie",
 "ieri",
 "il",
 "improvviso",
 "in",
 "inc",
 "indietro",
 "infatti",
 "inoltre",
 "insieme",
 "intanto",
 "intorno",
 "invece",
 "io",
 "l",
 "là",
 "lasciato",
 "lato",
 "lavoro",
 "le",
 "lei",
 "li",
 "lo",
 "lontano",
 "loro",
 "lui",
 "lungo",
 "luogo",
 "lì",
 "ma",
 "macche",
 "magari",
 "maggior",
 "mai",
 "male",
 "malgrado",
 "malissimo",
 "mancanza",
 "marche",
 "me",
 "medesimo",
 "mediante",
 "meglio",
 "meno",
 "mentre",
 "mesi",
 "mezzo",
 "mi",
 "mia",
 "mie",
 "miei",
 "mila",
 "miliardi",
 "milioni",
 "minimi",
 "ministro",
 "mio",
 "modo",
 "molta",
 "molte",
 "molti",
 "moltissimo",
 "molto",
 "momento",
 "mondo",
 "mosto",
 "nazionale",
 "ne",
 "negl",
 "negli",
 "nei",
 "nel",
 "nell",
 "nella",
 "nelle",
 "nello",
 "nemmeno",
 "neppure",
 "nessun",
 "nessuna",
 "nessuno",
 "niente",
 "no",
 "noi",
 "nome",
 "non",
 "nondimeno",
 "nonostante",
 "nonsia",
 "nostra",
 "nostre",
 "nostri",
 "nostro",
 "novanta",
 "nove",
 "nulla",
 "nuovi",
 "nuovo",
 "o",
 "od",
 "oggi",
 "ogni",
 "ognuna",
 "ognuno",
 "oltre",
 "oppure",
 "ora",
 "ore",
 "osi",
 "ossia",
 "ottanta",
 "otto",
 "paese",
 "parecchi",
 "parecchie",
 "parecchio",
 "parte",
 "partendo",
 "peccato",
 "peggio",
 "per",
 "perche",
 "perchè",
 "perché",
 "percio",
 "perciò",
 "perfino",
 "pero",
 "persino",
 "persone",
 "però",
 "piedi",
 "pieno",
 "piglia",
 "piu",
 "piuttosto",
 "più",
 "po",
 "pochissimo",
 "poco",
 "poi",
 "poiche",
 "possa",
 "possedere",
 "posteriore",
 "posto",
 "potrebbe",
 "preferibilmente",
 "presa",
 "press",
 "prima",
 "primo",
 "principalmente",
 "probabilmente",
 "promesso",
 "proprio",
 "puo",
 "pure",
 "purtroppo",
 "può",
 "qua",
 "qualche",
 "qualcosa",
 "qualcuna",
 "qualcuno",
 "quale",
 "quali",
 "qualunque",
 "quando",
 "quanta",
 "quante",
 "quanti",
 "quanto",
 "quantunque",
 "quarto",
 "quasi",
 "quattro",
 "quel",
 "quella",
 "quelle",
 "quelli",
 "quello",
 "quest",
 "questa",
 "queste",
 "questi",
 "questo",
 "qui",
 "quindi",
 "quinto",
 "realmente",
 "recente",
 "recentemente",
 "registrazione",
 "relativo",
 "riecco",
 "rispetto",
 "salvo",
 "sara",
 "sarai",
 "saranno",
 "sarebbe",
 "sarebbero",
 "sarei",
 "saremmo",
 "saremo",
 "sareste",
 "saresti",
 "sarete",
 "saro",
 "sarà",
 "sarò",
 "scola",
 "scopo",
 "scorso",
 "se",
 "secondo",
 "seguente",
 "seguito",
 "sei",
 "sembra",
 "sembrare",
 "sembrato",
 "sembrava",
 "sembri",
 "sempre",
 "senza",
 "sette",
 "si",
 "sia",
 "siamo",
 "siano",
 "siate",
 "siete",
 "sig",
 "solito",
 "solo",
 "soltanto",
 "sono",
 "sopra",
 "soprattutto",
 "sotto",
 "spesso",
 "srl",
 "sta",
 "stai",
 "stando",
 "stanno",
 "starai",
 "staranno",
 "starebbe",
 "starebbero",
 "starei",
 "staremmo",
 "staremo",
 "stareste",
 "staresti",
 "starete",
 "starà",
 "starò",
 "stata",
 "state",
 "stati",
 "stato",
 "stava",
 "stavamo",
 "stavano",
 "stavate",
 "stavi",
 "stavo",
 "stemmo",
 "stessa",
 "stesse",
 "stessero",
 "stessi",
 "stessimo",
 "stesso",
 "steste",
 "stesti",
 "stette",
 "stettero",
 "stetti",
 "stia",
 "stiamo",
 "stiano",
 "stiate",
 "sto",
 "su",
 "sua",
 "subito",
 "successivamente",
 "successivo",
 "sue",
 "sugl",
 "sugli",
 "sui",
 "sul",
 "sull",
 "sulla",
 "sulle",
 "sullo",
 "suo",
 "suoi",
 "tale",
 "tali",
 "talvolta",
 "tanto",
 "te",
 "tempo",
 "terzo",
 "th",
 "ti",
 "titolo",
 "torino",
 "tra",
 "tranne",
 "tre",
 "trenta",
 "triplo",
 "troppo",
 "trovato",
 "tu",
 "tua",
 "tue",
 "tuo",
 "tuoi",
 "tutta",
 "tuttavia",
 "tutte",
 "tutti",
 "tutto",
 "uguali",
 "ulteriore",
 "ultimo",
 "un",
 "una",
 "uno",
 "uomo",
 "va",
 "vai",
 "vale",
 "vari",
 "varia",
 "varie",
 "vario",
 "verso",
 "vi",
 "via",
 "vicino",
 "visto",
 "vita",
 "voi",
 "volta",
 "volte",
 "vostra",
 "vostre",
 "vostri",
 "vostro",
 "è",
 "è"];

var italian = {
	italian: italian_1
};

var dutch$1 = dutch.dutch;
var english$1 = english.english;
var french$1 = french.french;
var german$1 = german.german;
var spanish$1 = spanish.spanish;
var italian$1 = italian.italian;

var stopwords = {
	dutch: dutch$1,
	english: english$1,
	french: french$1,
	german: german$1,
	spanish: spanish$1,
	italian: italian$1
};

export default stopwords;
export { dutch$1 as dutch, english$1 as english, french$1 as french, german$1 as german, italian$1 as italian, spanish$1 as spanish };
