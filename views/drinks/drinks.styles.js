import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

:host{
	display: block;
	height: 100%;
	padding: 1rem;
}

label{
	display: inline-grid;
	grid-template-rows: 1fr 1fr;
}

.drinks{
	align-content: start;
	display: grid;
	grid-auto-rows: repeat( auto-fill, 5.5rem );
	grid-gap: 0.5rem;
	grid-template-columns: repeat( auto-fill, minmax( 16rem, 1fr ) );
	justify-content: space-around;
	margin-top: 1rem;
}

	`;
}
