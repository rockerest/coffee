import { translator } from "../../common/L10n.js";

var definitions = {
	"en-US": {
		"labels": {
			"search": "Search"
		}
	}
};

export function translations( locale ){
	return translator( locale, definitions );
}
