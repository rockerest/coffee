import { html } from "../../common/Component.js";

export function template( {
	drinks = [],
	updateSearchTerms,
	translations
} = {} ){
	return html`

<label>
	${translations.labels.search}
	<input @keyup="${updateSearchTerms}" />
</label>
<div class="drinks">
	${drinks.map( ( item ) => html`<coffee-item .item="${item}" type="${item.type}"></coffee-item>` )}
</div>

`;
}
