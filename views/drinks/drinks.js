import { english } from "../../web_modules/stopwords.js";
import stemmer from "../../web_modules/stemmer.js";
import leven from "../../web_modules/leven.js";

import { render, View } from "../../common/View.js";

import { publish, subscribeWith } from "../../common/MessageBus.js";

import { message as COFFEE_COFFEES_REQUEST } from "../../messages/COFFEE_COFFEES_REQUEST.js";
import { message as COFFEE_COFFEES_RECEIVE } from "../../messages/COFFEE_COFFEES_RECEIVE.js";
import { message as COFFEE_TEAS_REQUEST } from "../../messages/COFFEE_TEAS_REQUEST.js";
import { message as COFFEE_TEAS_RECEIVE } from "../../messages/COFFEE_TEAS_RECEIVE.js";
import { message as APP_MENU_ACTIVE } from "../../messages/APP_MENU_ACTIVE.js";

import { template } from "./drinks.template.js";
import { base, styles } from "./drinks.styles.js";
import { translations } from "./drinks.translations.js";

const MAXIMUM_MATCH_DISTANCE = 3;

function identity( input ){
	return input;
}

function getStems( text ){
	return text
		? text
			.split( " " )
			.filter( ( word ) => !english.includes( word.toLowerCase() ) )
			.map( stemmer )
		: [];
}

function getMatchedDrinks( drinks, searchStems, index ){
	var filtered = drinks;
	var indexedStems = Object.entries( index );

	if( searchStems.length ){
		let matchedIds = searchStems
			.map( ( stem ) => index[ stem ] )
			.flat()
			.filter( identity );

		let closeIds = searchStems
			.map( ( searchStem ) => indexedStems.map( ( [ indexedStem, ids ] ) => {
				let close;

				if( leven( indexedStem, searchStem ) <= MAXIMUM_MATCH_DISTANCE ){
					close = ids;
				}

				return close;
			} ) )
			.flat( 2 )
			.filter( identity );

		filtered = drinks.filter( ( drink ) => Array.from( new Set( [ ...matchedIds, ...closeIds ] ) ).includes( drink.id ) );
	}

	return filtered;
}

class Drinks extends View{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"coffees": { "type": Array },
			"teas": { "type": Array },
			"searchTerms": { "type": String, "reflect": true, "attribute": "search-terms" }
		};
	}

	get observe(){
		return {
			"teas": () => this.recomputeSearchIndex(),
			"coffees": () => this.recomputeSearchIndex()
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.coffees = [];
		this.teas = [];
		this.searchTerms = "";

		this.subscriptions = [];
		this.searchIndex = null;
		this.searchInput = null;
	}

	render(){
		var text = translations( this.locale );

		return template( {
			"updateSearchTerms": () => this.updateSearchTerms(),
			"drinks": this.getFiltered(),
			"translations": text
		} );
	}

	connectedCallback(){
		super.connectedCallback();

		publish( Object.assign( {}, APP_MENU_ACTIVE, { "active": "drinks" } ) );

		this.subscriptions.push( subscribeWith( {
			[COFFEE_COFFEES_RECEIVE.name]: ( message ) => {
				this.coffees = message.coffees;
			},
			[COFFEE_TEAS_RECEIVE.name]: ( message ) => {
				this.teas = message.teas;
			}
		} ) );
	}
	disconnectedCallback(){
		this.subscriptions.forEach( ( subscription ) => subscription.unsubscribe() );

		super.disconnectedCallback();
	}

	firstUpdated(){
		super.firstUpdated();

		this.searchInput = this.shadowRoot.querySelector( "input" );
	}

	updateSearchTerms(){
		this.searchTerms = this.searchInput.value;
	}

	recomputeSearchIndex(){
		this.searchIndex = {};

		[ ...this.coffees, ...this.teas ].forEach( ( drink ) => {
			this.searchIndex = Object.fromEntries(
				Object.entries(
					drink.name
						.split( " " )
						.filter( ( word ) => !english.includes( word.toLowerCase() ) )
						.reduce( ( stems, word ) => {
							let stem = stemmer( word );
							let stemBucket = stems[ stem ] || [];

							stemBucket.push( drink.id );

							stems[ stem ] = stemBucket;

							return stems;
						}, this.searchIndex )
				)
					.map( ( [ stem, ids ] ) => [ stem, Array.from( new Set( ids ) ) ] )
			);
		} );

		this.requestUpdate();
	}
	getFiltered(){
		var allDrinks = [ ...this.coffees, ...this.teas ];
		var filteredDrinks = this.searchIndex
			? getMatchedDrinks( allDrinks, getStems( this.searchTerms ), this.searchIndex )
			: allDrinks;

		return filteredDrinks;
	}
}

customElements.define( "view-drinks", Drinks );

export function view( config = {} ){
	var el = config.el;
	var renderedView = render( "view-drinks", el );

	renderedView.locale = config.details.locale;

	publish( COFFEE_COFFEES_REQUEST );
	publish( COFFEE_TEAS_REQUEST );
}
