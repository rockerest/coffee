import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

:host{
	align-content: start;
	display: grid;
	grid-gap: 0.5rem;
	grid-template-columns: repeat( auto-fill, minmax( 18rem, 1fr ) );
	height: 100%;
	justify-content: space-around;
	padding: 1rem;
}

	`;
}
