import { render, View } from "../../common/View.js";

import { publish, subscribeWith } from "../../common/MessageBus.js";

import { message as COFFEE_ITEMS_REQUEST } from "../../messages/COFFEE_ITEMS_REQUEST.js";
import { message as COFFEE_ITEMS_RECEIVE } from "../../messages/COFFEE_ITEMS_RECEIVE.js";
import { message as COFFEE_COFFEES_REQUEST } from "../../messages/COFFEE_COFFEES_REQUEST.js";
import { message as COFFEE_COFFEES_RECEIVE } from "../../messages/COFFEE_COFFEES_RECEIVE.js";
import { message as COFFEE_TEAS_REQUEST } from "../../messages/COFFEE_TEAS_REQUEST.js";
import { message as COFFEE_TEAS_RECEIVE } from "../../messages/COFFEE_TEAS_RECEIVE.js";
import { message as COFFEE_BUNDLES_REQUEST } from "../../messages/COFFEE_BUNDLES_REQUEST.js";
import { message as COFFEE_BUNDLES_RECEIVE } from "../../messages/COFFEE_BUNDLES_RECEIVE.js";
import { message as APP_MENU_ACTIVE } from "../../messages/APP_MENU_ACTIVE.js";

import { template } from "./bundles.template.js";
import { base, styles } from "./bundles.styles.js";

class Bundles extends View{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"items": { "type": Array },
			"coffees": { "type": Array },
			"teas": { "type": Array },
			"bundles": { "type": Array }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.items = [];
		this.coffees = [];
		this.teas = [];
		this.bundles = [];

		this.subscriptions = [];
	}

	render(){
		var items = [ ...this.items, ...this.coffees, this.teas ];
		var combinedBundles = this.bundles.map( ( bundle ) => Object.assign( {}, bundle, {
			"items": bundle.items.map( ( name ) => {
				let matchedItem = items.find( ( item ) => item.name == name );
				let mappedItem = { name };

				if( matchedItem ){
					mappedItem = matchedItem;
				}

				return mappedItem;
			} )
		} ) );

		return template( {
			"bundles": combinedBundles
		} );
	}

	connectedCallback(){
		super.connectedCallback();

		publish( Object.assign( {}, APP_MENU_ACTIVE, { "active": "bundles" } ) );

		this.subscriptions.push( subscribeWith( {
			[COFFEE_ITEMS_RECEIVE.name]: ( message ) => {
				this.items = message.items;
			},
			[COFFEE_COFFEES_RECEIVE.name]: ( message ) => {
				this.coffees = message.coffees;
			},
			[COFFEE_TEAS_RECEIVE.name]: ( message ) => {
				this.teas = message.teas;
			},
			[COFFEE_BUNDLES_RECEIVE.name]: ( message ) => {
				this.bundles = message.bundles;
			}
		} ) );
	}

	disconnectedCallback(){
		super.disconnectedCallback();

		this.subscriptions.forEach( ( subscription ) => subscription.unsubscribe() );
	}
}

customElements.define( "view-bundles", Bundles );

export function view( config = {} ){
	var el = config.el;
	var renderedView = render( "view-bundles", el );

	renderedView.locale = config.details.locale;

	publish( COFFEE_ITEMS_REQUEST );
	publish( COFFEE_COFFEES_REQUEST );
	publish( COFFEE_TEAS_REQUEST );
	publish( COFFEE_BUNDLES_REQUEST );
}
