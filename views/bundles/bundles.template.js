import { html } from "../../common/Component.js";

export function template( {
	bundles = []
} = {} ){
	return html`

${bundles.map( ( item ) => html`<coffee-bundle .bundle="${item}"></coffee-bundle>` )}

`;
}
