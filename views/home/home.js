import { render, View } from "../../common/View.js";

import { publish, subscribeWith } from "../../common/MessageBus.js";

import { message as COFFEE_ITEMS_REQUEST } from "../../messages/COFFEE_ITEMS_REQUEST.js";
import { message as COFFEE_ITEMS_RECEIVE } from "../../messages/COFFEE_ITEMS_RECEIVE.js";
import { message as APP_MENU_ACTIVE } from "../../messages/APP_MENU_ACTIVE.js";

import { template } from "./home.template.js";
import { base, styles } from "./home.styles.js";

class Home extends View{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"items": { "type": Array }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.items = [];

		this.subscriptions = [];
	}

	render(){
		return template( {
			"items": this.items
		} );
	}

	connectedCallback(){
		super.connectedCallback();

		publish( Object.assign( {}, APP_MENU_ACTIVE, { "active": "paraphernalia" } ) );

		this.subscriptions.push( subscribeWith( {
			[COFFEE_ITEMS_RECEIVE.name]: ( message ) => {
				this.items = message.items;
			}
		} ) );
	}

	disconnectedCallback(){
		super.disconnectedCallback();

		this.subscriptions.forEach( ( subscription ) => subscription.unsubscribe() );
	}
}

customElements.define( "view-home", Home );

export function view( config = {} ){
	var el = config.el;
	var renderedView = render( "view-home", el );

	renderedView.locale = config.details.locale;

	publish( COFFEE_ITEMS_REQUEST );
}
