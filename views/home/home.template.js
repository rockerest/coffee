import { html } from "../../common/Component.js";

export function template( {
	items = []
} = {} ){
	return html`

${items.map( ( item ) => html`<coffee-item .item="${item}" type="item"></coffee-item>` )}

`;
}
