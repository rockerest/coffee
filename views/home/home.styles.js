import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

:host{
	align-content: start;
	display: grid;
	grid-gap: 0.5rem;
	grid-template-columns: repeat( auto-fill, minmax( 16rem, 1fr ) );
	grid-auto-rows: repeat( auto-fill, 5.5rem );
	height: 100%;
	justify-content: space-around;
	padding: 1rem;
}

	`;
}
