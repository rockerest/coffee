import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

	:host{
		display: block;
	}

	ul{
		display: inline-block;
		margin: 0;
		padding: 0;
	}

	li{
		display: inline-flex;
	}
	
	a{
		color: var( --color-text-default );
		padding: 0.5rem 1.5rem;
		text-decoration: none;
	}

	.active a{
		border-bottom: 1px solid var( --color-border );
	}
`;
}
