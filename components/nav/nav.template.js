import { html } from "../../common/Component.js";

export function template( {
	active,
	text
} = {} ){
	var isActive = ( check ) => ( check == active ? "active" : "" );

	return html`

<nav>
	<ul>
		<li class="${isActive( "paraphernalia" )}">
			<a href="/">${text.paraphernalia}</a>
		</li>
		<li class="${isActive( "drinks" )}">
			<a href="/drinks">${text.drinks}</a>
		</li>
		<li class="${isActive( "bundles" )}">
			<a href="/bundles">${text.bundles}</a>
		</li>
	</ul>
</nav>

`;
}
