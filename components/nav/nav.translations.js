import { translator } from "../../common/L10n.js";

var definitions = {
	"en-US": {
		"paraphernalia": "Paraphernalia",
		"drinks": "Drinks",
		"bundles": "Bundles"
	}
};

export function translations( locale ){
	return translator( locale, definitions );
}
