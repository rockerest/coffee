import { Component } from "../../common/Component.js";

import { subscribeWith } from "../../common/MessageBus.js";

import { message as APP_MENU_ACTIVE } from "../../messages/APP_MENU_ACTIVE.js";

import { template } from "./nav.template.js";
import { base, styles } from "./nav.styles.js";
import { translations } from "./nav.translations.js";
export class Nav extends Component{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"active": { "type": String }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.active = "paraphernalia";

		this.subscriptions = [];
	}

	connectedCallback(){
		super.connectedCallback();

		this.subscriptions.push( subscribeWith( {
			[APP_MENU_ACTIVE.name]: ( message ) => {
				this.active = message.active;
			}
		} ) );
	}

	disconnectedCallback(){
		super.disconnectedCallback();

		this.subscriptions.forEach( ( subscription ) => subscription.unsubscribe() );
	}

	render(){
		var localizedText = translations( this.locale );

		return template( {
			"active": this.active,
			"text": localizedText
		} );
	}
}