import { Component } from "../../common/Component.js";

import { template } from "./bundle.template.js";
import { base, styles } from "./bundle.styles.js";

export class Bundle extends Component{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"bundle": { "type": Object }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.bundle = {};
	}

	render(){
		return template( {
			"bundle": this.bundle
		} );
	}
}