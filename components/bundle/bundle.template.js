import { html } from "../../common/Component.js";

export function template( {
	bundle = {}
} = {} ){
	return html`

<h3>${bundle.name}</h3>
<div class="items">
	${bundle.items.map( ( item ) => html`<coffee-item .item="${item}" type="${item.type}"></coffee-item>` )}
</div>

`;
}
