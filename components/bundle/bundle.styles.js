import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

:host{
	--coffee-item-background-color: var( --color-background );

	border-radius: var( --size-radius-default );
	display: inline-block;
	margin-top: 1rem;
	max-height: max-content;
	min-height: 35vh;
	overflow: hidden auto;
	padding: 1rem;
	width: 18rem;
}

.items{
	align-content: start;
	display: grid;
	grid-gap: 0.5rem;
	grid-template-columns: 1fr;
	justify-content: space-around;
}

	`;
}
