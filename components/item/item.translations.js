import { translator } from "../../common/L10n.js";

var definitions = {
	"en-US": {
		"tooltips": {
			"coffee": "Coffee",
			"tea": "Tea",
			"tool": "Tool"
		}
	}
};

export function translations( locale ){
	return translator( locale, definitions );
}