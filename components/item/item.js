import { Component } from "../../common/Component.js";

import { template, display } from "./item.template.js";
import { base, styles } from "./item.styles.js";
import { translations } from "./item.translations.js";

function getIcon( item ){
	var icon = "tools";
	var icons = {
		"coffee": "coffee-pot",
		"tea": "mug-tea"
	};

	if( icons[ item.type ] ){
		icon = icons[ item.type ];
	}

	return icon;
}

export class Item extends Component{
	static get styles(){
		return [
			base(),
			styles()
		];
	}

	static get properties(){
		return {
			"locale": { "type": String, "reflect": true },
			"item": { "type": Object }
		};
	}

	constructor( ...args ){
		super( ...args );

		this.locale = "en-US";
		this.item = {};
	}

	render(){
		var text = translations( this.locale );
		var iconTooltip = text.tooltips[ this.item.type ] || text.tooltips.tool;

		return template( {
			"item": this.item,
			"displayName": display( {
				"text": this.item.name,
				"link": this.item.link
			} ),
			"icon": getIcon( this.item ),
			iconTooltip
		} );
	}
}