import { html } from "../../common/Component.js";

export function template( {
	item = {},
	displayName,
	icon,
	iconTooltip
} = {} ){
	return html`

<div class="content">
	${item.image ? html`<figure>
		<picture>
			<img src="${item.image}" />
		</picture>
	</figure>` : ""}
	${displayName}
	<coffee-icon type="duotone" icon="${icon}" title="${iconTooltip}" fixed-width></coffee-icon>
</div>

`;
}

export function display( {
	text,
	link
} = {} ){
	var linkText = html`<span><a href="${link}">${text}</a></span>`;
	var plainText = html`<span>${text}</span>`;

	return link ? linkText : plainText;
}
