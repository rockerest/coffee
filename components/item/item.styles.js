import { css } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function styles(){
	return css`

:host{
	border-radius: var( --size-radius-default );
	height: calc( 5.5rem + 2px );
	padding: 1px;
	width: 16rem;
}

:host *{
	box-sizing: border-box;
}

:host([type="coffee"]),
:host([type="tea"]){
	background:
		radial-gradient( circle at 0% 0%, var( --color-coffee ), transparent 25% ),
		radial-gradient( circle at 100% 100%, var( --color-coffee ), transparent 25% );
}
:host([type="coffee"]) coffee-icon,
:host([type="tea"]) coffee-icon{
	--fa-primary-color: var( --color-coffee );
}

:host([type="item"]){
	background:
		radial-gradient( circle at 0% 0%, var( --color-item ), transparent 25% ),
		radial-gradient( circle at 100% 100%, var( --color-item ), transparent 25% );
}
:host([type="item"]) coffee-icon{
	--fa-primary-color: var( --color-item );
}

.content{
	align-items: center;
	background-color: var( --color-background );
	border-radius: var( --size-radius-default );
	color: var( --color-text-default );
	display: inline-grid;
	grid-gap: 0.5rem;
	grid-template-columns: 3.5rem 1fr;
	height: 100%;
	padding: 1rem;
	position: relative;
	width: 100%;
}

.content > coffee-icon{
	bottom: 0.25rem;
	position: absolute;
	right: 0.25rem;
}

a{
	color: inherit;
}

figure{
	height: 3.5rem;
	margin: 0;
	width: 3.5rem;
}

figure picture img{
	border: 1px solid var( --color-border );
	border-radius: 50%;
	display: inline-block;
	height: 3.5rem;
	margin: auto;
	width: 3.5rem;
}

	`;
}
