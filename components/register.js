import { Item as ItemComponent } from "./item/item.js";
import { Nav as NavComponent } from "./nav/nav.js";
import { Bundle as BundleComponent } from "./bundle/bundle.js";
import { Icon as IconComponent } from "./icon/icon.js";

export function register(){
	customElements.define( "site-nav", NavComponent );
	customElements.define( "coffee-item", ItemComponent );
	customElements.define( "coffee-bundle", BundleComponent );
	customElements.define( "coffee-icon", IconComponent );
}