import { dom } from "../../web_modules/@fortawesome/fontawesome-svg-core.js";

import { css, unsafeCSS } from "../../common/Component.js";
import { base } from "../../common/CSS.js";

export { base };

export function fa(){
	return css`${unsafeCSS( dom.css() )}`;
}

export function styles(){
	return css`

:host{
	display: inline-block;
}

span.icon{
    line-height: 1em;
}

	`;
}
