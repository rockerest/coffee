import { Component } from "../../common/Component.js";

import { library, icon as iconParser, parse } from "../../web_modules/@fortawesome/fontawesome-svg-core.js";

var fadCoffeePot = { "prefix": "fad", "iconName": "coffee-pot", "icon": [ 512, 512, [], "f902", [ "M424.06,171.86l-271.86.63C98.63,212.07,64,274.39,64,344.62c0,51.47,18.62,84.77,49.64,117.71A57.17,57.17,0,0,0,155.16,480H420.84a57.08,57.08,0,0,0,41.45-17.6c31-32.95,49.69-66.24,49.71-117.72C512,275.13,478.41,211.82,424.06,171.86Zm-6,244.14H158c-22.34-24.35-30-42.72-30-71.38,0-41.12,17-79.62,47.3-108.18l225.81-.53A148.57,148.57,0,0,1,448,344.66C448,373.3,440.37,391.67,418.05,416Z", "M130.55,320A146,146,0,0,0,128,344.62c0,28.66,7.62,47,30,71.38H418.05C440.37,391.67,448,373.3,448,344.66A150.45,150.45,0,0,0,445.5,320ZM424.06,171.86,480,32H88A88,88,0,0,0,0,120v88a16,16,0,0,0,16,16H48a16,16,0,0,0,16-16V136a40,40,0,0,1,40-40h17.6l30.6,76.49Z" ] ] };
var fadMugTea = { "prefix": "fad", "iconName": "mug-tea", "icon": [ 640, 512, [], "f875", [ "M595.6 416H12.35c-25 0-11.59 64 36 64h511.36c47.69 0 60.89-64 35.89-64zM192 256h64a32 32 0 0 0 32-32v-66.75a32 32 0 0 0-9.38-22.62L240 96V32h-32v64l-38.63 38.63a32 32 0 0 0-9.37 22.62V224a32 32 0 0 0 32 32z", "M512 32H240v64l38.62 38.63a32 32 0 0 1 9.38 22.62V224a32 32 0 0 1-32 32h-64a32 32 0 0 1-32-32v-66.75a32 32 0 0 1 9.37-22.62L208 96V32h-87.95a23.94 23.94 0 0 0-24 24v232a96 96 0 0 0 96 96H384a96 96 0 0 0 96-96h32a128 128 0 0 0 0-256zm0 192h-32V96h32a64 64 0 0 1 0 128z" ] ] };
var fadTools = { "prefix": "fad", "iconName": "tools", "icon": [ 512, 512, [], "f7d9", [ "M193.8 227.7L18.74 402.76a64 64 0 0 0 90.5 90.5l148.88-148.88a75.36 75.36 0 0 1 6.58-45.78zM64 472a24 24 0 1 1 24-24 24 24 0 0 1-24 24zm443.73-362.9a12 12 0 0 0-20.12-5.51L413.25 178l-67.88-11.31-11.31-67.93 74.36-74.36a12 12 0 0 0-5.66-20.16 143.92 143.92 0 0 0-136.58 37.93c-39.64 39.64-50.55 97.1-34.05 147.2l-4.43 4.43 70.9 70.9a74.25 74.25 0 0 1 85.4 13.9l7.21 7.21a141.74 141.74 0 0 0 78.61-40 143.94 143.94 0 0 0 37.91-136.71z", "M501.1 395.7a37.36 37.36 0 0 1 0 52.7l-52.7 52.7a37.18 37.18 0 0 1-52.58.12l-.12-.12L278.6 384c-23.1-23.1-27.5-57.6-13.9-85.4L158.1 192H96L0 64 64 0l128 96v62.1l106.6 106.6a74.25 74.25 0 0 1 85.4 13.9z" ] ] };

import { template } from "./icon.template.js";
import { base, fa, styles } from "./icon.styles.js";

library.add(
	fadCoffeePot,
	fadMugTea,
	fadTools
);

function getClasses( { spin, fixedWidth, duoSwap } = {} ){
	var classes = {
		"fa-spin": spin,
		"fa-fw": fixedWidth,
		"fa-swap-opacity": duoSwap
	};

	return Object
		.entries( classes )
		.filter( ( [ , on ] ) => on )
		.map( ( [ className ] ) => className );
}

function getSvg( { type, icon, transforms, title, classes } = {} ){
	var packs = {
		"light": "fal",
		"brands": "fab",
		"solid": "fas",
		"regular": "far",
		"duotone": "fad"
	};
	var svg = {
		"prefix": packs[ type ],
		"iconName": icon
	};

	return iconParser( svg, {
		"transform": parse.transform( transforms ),
		title,
		classes
	} ).html;
}

export class Icon extends Component{
	static get styles(){
		return [
			base(),
			fa(),
			styles()
		];
	}
	static get properties(){
		return {
			"type": { "type": String, "reflect": true },
			"spin": { "type": Boolean, "reflect": true },
			"fixedWidth": { "type": Boolean, "attribute": "fixed-width", "reflect": true },
			"duoSwap": { "type": Boolean, "attribute": "swap", "reflect": true },
			"title": { "type": String, "reflect": true },
			"icon": { "type": String, "reflect": true },
			"transforms": { "type": String, "reflect": true }
		};
	}

	constructor(){
		var self = super();

		self.type = "light";
		self.spin = false;
		self.fixedWidth = false;
		self.duoSwap = false;
		self.title = "";
		self.icon = "";
		self.transforms = "";
	}

	render(){
		return template( {
			"svg": getSvg( {
				"type": this.type,
				"icon": this.icon,
				"transforms": this.transforms,
				"title": this.title,
				"classes": getClasses( {
					"spin": this.spin,
					"fixedWidth": this.fixedWidth,
					"duoSwap": this.duoSwap
				} )
			} )
		} );
	}
}
