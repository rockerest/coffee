import { html, unsafeHTML } from "../../common/Component.js";

export function template( { svg } = {} ){
	return html`
<span class="icon">${unsafeHTML( svg )}</span>
`;
}
