import { uuidv4 } from "../web_modules/@bundled-es-modules/uuid.js";

import { ajax } from "../common/Ajax.js";

import { message as COFFEE_ITEMS_REQUEST } from "../messages/COFFEE_ITEMS_REQUEST.js";
import { message as COFFEE_ITEMS_RECEIVE } from "../messages/COFFEE_ITEMS_RECEIVE.js";
import { message as COFFEE_COFFEES_REQUEST } from "../messages/COFFEE_COFFEES_REQUEST.js";
import { message as COFFEE_COFFEES_RECEIVE } from "../messages/COFFEE_COFFEES_RECEIVE.js";
import { message as COFFEE_TEAS_REQUEST } from "../messages/COFFEE_TEAS_REQUEST.js";
import { message as COFFEE_TEAS_RECEIVE } from "../messages/COFFEE_TEAS_RECEIVE.js";
import { message as COFFEE_BUNDLES_REQUEST } from "../messages/COFFEE_BUNDLES_REQUEST.js";
import { message as COFFEE_BUNDLES_RECEIVE } from "../messages/COFFEE_BUNDLES_RECEIVE.js";

export function register( publish, subscribe ){
	subscribe( {
		[COFFEE_ITEMS_REQUEST.name]: () => {
			ajax.request( {
				"method": "GET",
				"url": `${window.location.protocol}//${window.location.host}/data/items.json`
			} )
				.then( ( response ) => {
					if( response.ok ){
						publish( Object.assign(
							{},
							COFFEE_ITEMS_RECEIVE,
							{
								"items": response.content.map( ( item ) => ( {
									...item,
									"id": uuidv4(),
									"type": "item"
								} ) )
							}
						) );
					}
				} );
		},
		[COFFEE_COFFEES_REQUEST.name]: () => {
			ajax.request( {
				"method": "GET",
				"url": `${window.location.protocol}//${window.location.host}/data/coffee.json`
			} )
				.then( ( response ) => {
					if( response.ok ){
						publish( Object.assign(
							{},
							COFFEE_COFFEES_RECEIVE,
							{
								"coffees": response.content.map( ( coffee ) => ( {
									...coffee,
									"id": uuidv4(),
									"type": "coffee"
								} ) )
							}
						) );
					}
				} );
		},
		[COFFEE_TEAS_REQUEST.name]: () => {
			ajax.request( {
				"method": "GET",
				"url": `${window.location.protocol}//${window.location.host}/data/tea.json`
			} )
				.then( ( response ) => {
					if( response.ok ){
						publish( Object.assign(
							{},
							COFFEE_TEAS_RECEIVE,
							{
								"teas": response.content.map( ( tea ) => ( {
									...tea,
									"id": uuidv4(),
									"type": "tea"
								} ) )
							}
						) );
					}
				} );
		},
		[COFFEE_BUNDLES_REQUEST.name]: () => {
			ajax.request( {
				"method": "GET",
				"url": `${window.location.protocol}//${window.location.host}/data/bundles.json`
			} )
				.then( ( response ) => {
					if( response.ok ){
						publish( Object.assign(
							{},
							COFFEE_BUNDLES_RECEIVE,
							{
								"bundles": Object
									.entries( response.content )
									.reduce( ( collection, [ name, items ] ) => {
										collection.push( {
											"id": uuidv4(),
											"type": "bundle",
											items,
											name
										} );

										return collection;
									}, [] )
							} ) );
					}
				} );
		}
	} );
}
