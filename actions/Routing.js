import page from "../web_modules/page.js";

import { register as registerPreRoute } from "../routers/pre.js";
import { register as registerHome } from "../routers/home.js";

import { message as ROUTING_INIT } from "../messages/ROUTING_INIT.js";
import { message as ROUTING_READY } from "../messages/ROUTING_READY.js";
import { message as ROUTING_START } from "../messages/ROUTING_START.js";

export function register( publish, subscribe ){
	subscribe( {
		[ROUTING_INIT.name]: () => {
			let router = page;

			window[ window.NAMESPACE ].router = router;

			registerPreRoute( router, publish );
			registerHome( router, publish );

			publish( ROUTING_READY );
		},
		[ROUTING_READY.name]: () => {
			publish( ROUTING_START );
		},
		[ROUTING_START.name]: () => {
			window[ window.NAMESPACE ].router();
		}
	} );
}
