import { message as VIEW_CHANGE } from "../messages/VIEW_CHANGE.js";

import { view as home } from "../views/home/home.js";
import { view as drinks } from "../views/drinks/drinks.js";
import { view as bundles } from "../views/bundles/bundles.js";

var views = {
	home,
	drinks,
	bundles
};

export function register( publish, subscribe ){
	subscribe( {
		[VIEW_CHANGE.name]: ( message ) => {
			let el = message.el || document.body;

			if( views[ message.view ] ){
				views[ message.view ]( {
					"context": message.context,
					"details": message.details,
					el
				} );
			}
		}

	} );
}
