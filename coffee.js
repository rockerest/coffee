import { start as startMessageBus, publish, subscribeWith } from "./common/MessageBus.js";
import { register as registerComponents } from "./components/register.js";

import { get } from "./common/storage/local.js";
import { log } from "./common/Console.js";
import { parse } from "./common/Json.js";

import { register as registerRoutingActions } from "./actions/Routing.js";
import { register as registerViewActions } from "./actions/View.js";
import { register as registerCoffeeActions } from "./actions/Coffee.js";

import { message as ROUTING_INIT } from "./messages/ROUTING_INIT.js";

var bus;
var partialPublish;
var partialSubscribe;

window.NAMESPACE = "coffee";
window[ window.NAMESPACE ] = {};

bus = startMessageBus( window );

partialPublish = ( ...args ) => publish( ...args, bus );
partialSubscribe = ( ...args ) => subscribeWith( ...args, bus );

registerComponents( partialPublish, partialSubscribe );

registerRoutingActions( partialPublish, partialSubscribe );
registerViewActions( partialPublish, partialSubscribe );
registerCoffeeActions( partialPublish, partialSubscribe );

partialSubscribe( {
	"**": ( msg ) => {
		let debug = get( "debug" );

		if( debug && parse( debug ) ){
			log( msg );
		}
	}
} );


publish( ROUTING_INIT );
