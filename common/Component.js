import { css, unsafeCSS, html, LitElement } from "../web_modules/lit-element.js";
import { unsafeHTML } from "../web_modules/lit-html/directives/unsafe-html.js";

export class Component extends LitElement{
	updated( ...args ){
		var changed = args[ 0 ];

		super.updated( ...args );

		if( this.observe ){
			changed.forEach( ( oldValue, key ) => {
				if( this.observe[ key ] ){
					this.observe[ key ]( oldValue, this[ key ] );
				}
			} );
		}
	}

	emit( type, detail ){
		this.dispatchEvent( new CustomEvent( type, { detail } ) );
	}
}

export { html, css, unsafeHTML, unsafeCSS };
