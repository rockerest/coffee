import { uuidv4 } from "../web_modules/@bundled-es-modules/uuid.js";
import { Subject } from "../web_modules/rxjs.js";

import { subscribeWith as internalSubscribeWith } from "./Observable.js";

export function start( global = window ){
	global[ global.NAMESPACE ].bus = new Subject();

	return global[ global.NAMESPACE ].bus;
}

export function get( global = window ){
	return global[ global.NAMESPACE ].bus;
}

export function publish( msg, bus = get() ){
	var newMsg = Object.assign( transaction( msg ), msg );

	bus.next( newMsg );

	return newMsg;
}

export function subscribeWith( map = {}, bus = get() ){
	return bus.subscribe( internalSubscribeWith( map ) );
}

export function once( message, action, bus = get() ){
	var stream;
	var map = {
		[message]: ( ...args ) => {
			action( ...args );

			stream.unsubscribe();
		}
	};

	stream = subscribeWith( map, bus );

	return stream;
}

export function transaction( message = {}, forceNewRoot = false ){
	var tx = uuidv4();
	var newTx = {};

	if( message.tx ){
		newTx.tx = forceNewRoot ? tx : message.tx;
	}
	else{
		newTx.tx = tx;
	}

	return newTx;
}
