import { message as VIEW_CHANGE } from "../messages/VIEW_CHANGE.js";

import { Component } from "./Component.js";

export function render( name, el = document.body ){
	var renderedNow = el.renderedNow;
	var view;

	if( renderedNow == name ){
		view = el.querySelector( name );
	}
	else{
		view = document.createElement( name );

		el.innerHTML = "";
		el.appendChild( view );
		el.renderedNow = name;
	}

	return view;
}

export function viewChange( ctx ){
	return Object.assign( {}, VIEW_CHANGE, {
		"context": ctx,
		"el": ctx.el
	} );
}

export class View extends Component{}
