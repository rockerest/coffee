function getHandler( value, dictionary ){
	var handler = dictionary[ value ]; // if value is a string, try it as a key

	if( value && value.name ){
		handler = dictionary[ value.name ]; // otherwise use value.name to associate events to handlers
	}

	return handler;
}

export function subscribeWith( dictionary, handlers ){
	if( typeof dictionary == "string" && handlers ){
		dictionary = {
			[dictionary]: handlers
		};
	}

	dictionary = Object.assign( {}, {
		"*": () => {}
	}, dictionary );

	return ( value ) => {
		let handler = getHandler( value, dictionary ) || dictionary[ "*" ]; // "*" is a fallback for any unhandled event

		handler( value );

		if( dictionary[ "**" ] ){
			dictionary[ "**" ]( value ); // "**" is an "always" handler for any event
		}
	};
}
