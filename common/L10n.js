import { get } from "./storage/local.js";

function getFirstLanguage( navigator ){
	return ( navigator.languages && navigator.languages[ 0 ] ) || navigator.language;
}

function resolveLanguages( navigator ){
	return getFirstLanguage( navigator ) || navigator.userLanguage;
}

function getFallbackLocale(){
	return typeof navigator === "undefined" ? "en-US" : resolveLanguages( navigator );
}

function L10n( { locale = getFallbackLocale() } = {} ){
	this.locale = locale;
}

L10n.prototype.loadDefinitions = function loadDefinitions( definition ){
	this.translations = definition;
};

L10n.prototype.localize = function localize(){
	var response = this.translations[ "en-US" ];
	var tag = this.locale;
	var parts = tag.split( "-" );

	function generateIncreasinglySpecificLanguageTags( partsParam ){
		var thisTags = [];
		var i = 0;
		var ending;

		for( i; i < partsParam.length; i++ ){
			ending = undefined;

			if( partsParam.length - 1 > i ){
				ending = i - -1;
			}

			thisTags.push( partsParam.slice( 0, ending ).join( "-" ) );
		}

		return thisTags;
	}

	generateIncreasinglySpecificLanguageTags( parts )
		.forEach( ( specificTag ) => {
			if( this.translations[ specificTag ] ){
				response = this.translations[ specificTag ];
			}
		} );

	return response;
};

export function translator( locale, definitions ){
	var l10n = new L10n( { locale } );

	l10n.loadDefinitions( definitions );

	return l10n.localize();
}

export function getLocale(){
	return get( "locale" ) || getFallbackLocale() || "en-US";
}

export { L10n, getFallbackLocale };
