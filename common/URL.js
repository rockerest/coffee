export function getSearch( search = String( window.location.search ) ){
	search = search
		.replace( /^\?/, "" ) // remove leading question mark
		.replace( /#.*$/, "" ); // remove anything after the first octothorpe

	return search
		.split( "&" )
		.map( ( item ) => item.split( "=" ) )
		.filter( ( [ k, v ] ) => k || v )
		.reduce( ( p, [ k, v ] ) => {
			try{
				p[ k ] = JSON.parse( v );
			}
			catch( e ){
				p[ k ] = v;
			}

			return p;
		}, {} );
}