export function log( ...args ){
	console.log( ...args ); // eslint-disable-line no-console
}

export function error( ...args ){
	console.error( ...args ); // eslint-disable-line no-console
}

export function table( ...args ){
	console.table( ...args ); // eslint-disable-line no-console
}
