export function get( key ){
	return localStorage.getItem( key );
}

export function set( key, val ){
	return localStorage.setItem( key, val );
}

export function remove( key ){
	return localStorage.removeItem( key );
}
