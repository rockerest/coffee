import { viewChange } from "../common/View.js";

export function register( app, publish ){
	app( "/", ( ctx ) => {
		let msg = viewChange( ctx );

		msg.view = "home";

		publish( msg );
	} );

	app( "/drinks", ( ctx ) => {
		let msg = viewChange( ctx );

		msg.view = "drinks";

		publish( msg );
	} );

	app( "/bundles", ( ctx ) => {
		let msg = viewChange( ctx );

		msg.view = "bundles";

		publish( msg );
	} );
}